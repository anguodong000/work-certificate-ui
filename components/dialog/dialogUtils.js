import dialog from "@/components/dialog/dialog.js"
module.exports = {
	/**
	 * 弹出提示
	 */
	alert(content = "", title = "提示", callback, confirmText = '确定') {
		// #ifdef APP-PLUS
		dialog.alert({
			content,
			title,
			confirmText
		}, callback)
		// #endif
		// #ifndef APP-PLUS
		uni.showModal({
			title,
			content,
			confirmText,
			showCancel: false,
			confirmColor: "#e03c31",
			success: callback
		})
		// #endif
	},
	/**
	 * 确认提示框
	 */
	confirm(content = "", confirm, cancel, confirmText = '确定', cancelText = '取消', title = "提示") {
		// #ifdef APP-PLUS
		dialog.confirm({
			content,
			title,
			confirmText,
			cancelText,
		}, confirm, cancel)
		// #endif
		// #ifndef APP-PLUS
		uni.showModal({
			title,
			content,
			cancelText,
			confirmText,
			confirmColor: "#e03c31",
			success: (e) => {
				if (e.confirm) {
					confirm && confirm()
				} else if (e.cancel) {
					cancel && cancel()
				}
			},
			fail: (e) => {
				console.log(e)
			}
		})
		// #endif
	},
	/**
	 * 确认提示框
	 * @property {String} content 对话框内容
	 * @property {function} confirm 对话框内容
	 */
	dialogConfirm(content = "", confirm, cancel, confirmText = '确定', cancelText = '取消', msgType ='info', title = "提示") {
		// #ifdef APP-PLUS
		dialog.dialogConfirm({
			content,
			title,
			confirmText,
			cancelText,
			msgType
		}, confirm, cancel)
		// #endif
		// #ifndef APP-PLUS
		uni.showModal({
			title,
			content,
			cancelText,
			confirmText,
			confirmColor: "#e03c31",
			success: (e) => {
				if (e.confirm) {
					confirm && confirm()
				} else if (e.cancel) {
					cancel && cancel()
				}
			},
			fail: (e) => {
				console.log(e)
			}
		})
		// #endif
	},
	showMessage(messageText, msgType="success") {
		// #ifdef APP-PLUS
		dialog.showMessage({
			msgType,
			messageText
		})
		// #endif
		// #ifndef APP-PLUS
		uni.showToast({
			title: messageText,
			icon: 'none'
		})
		// #endif
	},
	popupLoadingOpen(popupContent = "加载中...") {
		// #ifdef APP-PLUS
		dialog.popupLoadingOpen({
			popupContent
		})
		// #endif
		// #ifndef APP-PLUS
		uni.showToast({
			title: content,
			icon: 'none'
		})
		// #endif
	},
	popupLoadingClose() {
		dialog.back(true);
	}
}