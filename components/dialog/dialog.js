
export default {
	/* 链接处理 */
	getLink(params) {
		let url = "/components/dialog/index";
		if (params) {
			let paramStr = "";
			for (let name in params) {
				paramStr += `&${name}=${params[name]}`
			}
			if (paramStr) {
				url += `?${paramStr.substr(1)}`
			}
		}
		return url;
	},
	// 将URL参数分割为对象键值对
	getParam(curParam){
	  // 拼接参数
	  let param = ''
	  for (let key in curParam) {
	    param += '&' + key + '=' + curParam[key]
	  }
	                
	  // 把参数保存为对像
	  let obj = {}
	  for (let key in curParam) {
	    obj[key] = curParam[key]
	  }
	  return obj
	},
	/* APP全局弹窗 */
	dialog(params = {}, callback) {
		this.back();
		uni.navigateTo({
			url: this.getLink(params),
			success(e) {
				if (callback != null && typeof callback == "function") {
					uni.$off("zy_common_dialog");
					uni.$on("zy_common_dialog", (type) => {
						callback && callback(type)
					})
				}
			}
		})
	},
	/*弹出提示弹窗  */
	alert(data = {}, callback, close) {
		let params = {
			dialogType: "alert",
			isCloseBtn: '0',
			isMaskClose: '0',
			isShow:true,
			...data
		};
		this.dialog(params, (type) => {
			if ("confirm" == type) {
				callback && callback()
			} else {
				close && close()
			}
		})
	},
	/*确认提示框弹窗 */
	confirm(data = {}, confirm, cancel, close) {
		let params = {
			dialogType: "confirm",
			isCloseBtn: '0',
			isMaskClose: '0',
			isShow:true,
			...data
		};
		this.dialog(params, (type) => {
			if ("confirm" == type) {
				confirm && confirm()
			} else if ("cancel" == type) {
				cancel && cancel()
			} else if ("close" == type) {
				close && close()
			}
		})
	},
	/*确认提示框弹窗 */
	dialogConfirm(data = {}, confirm, cancel, close) {
		let params = {
			dialogType: "dialogOpen",
			...data
		};
		this.dialog(params, (type) => {
			if ("confirm" == type) {
				confirm && confirm()
			} else if ("cancel" == type) {
				cancel && cancel()
			} else if ("close" == type) {
				close && close()
			}
		})
	},
	/*消息提示框 */
	showMessage(data = {}) {
		let params = {
			dialogType: "messageOpen",
			isMaskClose: '1',
			...data
		};
		this.dialog(params)
	},
	/**
	 * 加载框
	 */
	popupLoadingOpen(data = {}) {
		let params = {
			dialogType: "popupLoadingOpen",
			...data
		};
		this.dialog(params)
	},
	back(isCheckPopupLoading=false){
		//保证唯一弹窗
		let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
		if(routes.length>1){
			let curRoute = routes[routes.length - 1].route //获取当前页面路由
			if(curRoute=="components/dialog/index"){
				if(isCheckPopupLoading){
					let curParam = routes[routes.length - 1].options; //获取路由参数
					let paramObj=this.getParam(curParam);
					if(paramObj.dialogType=="popupLoadingOpen")
					uni.navigateBack();
				}else{
					uni.navigateBack();
				}
			}
		}
	}
}