const defaults ={
	msgType:'success',
	messageText:''
}

import Vue from 'vue'
import msgboxVue from './index.vue'

const MessageBoxConstructor = Vue.extend(msgboxVue)
let instance;

function initInstance(){
	instance = new MessageBoxConstructor({
		el: document.createElement('div')
	});
	document.body.appendChild(instance.$el);
}

function showNextMsg(options){
	if(!instance){
		initInstance()
	}
	for (let prop in options) {
	        // if (options.hasOwnProperty(prop)) {
	         
	        // }
			 instance[prop] = options[prop];
	      }
	Vue.nextTick(()=>{
		instance.show()
	})
	
}

const initMessageBox = function(option, callback){
	// if (typeof options === 'string') {
	//     options = {
	//       messageText: options
	//     };
	//   } else if (options.callback && !callback) {
	//     callback = options.callback;
	//   }
	  // 展示信息
	  showNextMsg(option)
	
}
const MessageBox ={
	alert: (option) => {
		console.log('MessageBox', option)
		initMessageBox(option)
	}
};

export default MessageBox;
export {MessageBox}
