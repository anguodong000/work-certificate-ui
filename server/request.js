import {basicUrl} from './config.js'
function requestApi (url, data, header={},method){
	const token = uni.getStorageSync('token')
	let initHeader ={}
	if(url != '/login' && url != '/register' && url !='/sendPhoneCode' && token){
		initHeader.Authorization = token
	}
	let promise = new Promise((resolve,reject)=>{
		uni.request({
			url: basicUrl+url,
			data:data,
			method:method,
			header:{
				...initHeader,
				...header
			}
		}).then(res =>{
			if(res.data.code == 200){
				resolve(res.data)
			}else if(res.data.code == 401){
				uni.MessageBox.alert({messageText:res.data.message,msgType:'error'})
				promise.reject(res.data)
				//  权限失效，登出，返回登录页面
				uni.navigateTo({
					url:'/pages/index/index'
				})
			}else{
				uni.MessageBox.alert({messageText:res.data.message,msgType:'error'})
				promise.reject(res.data)
			}
		}).catch(err=>{
			reject(err)
		})
	})
	return promise
}
function upFileApi(url,data){
	const token = uni.getStorageSync('token')
	// alert(JSON.stringify(data.file.tempFiles[0]))
		let promise = new Promise((resolve,reject)=>{
			// alert('success')
			uni.uploadFile({
				timeout:120000,
				url: basicUrl+url,
				file: data.file.tempFiles[0],
				formData:{
					fileType: data.fileType
				},
				header:{
					Authorization: token
				},
				success: (res) => {
					console.log('success',res)
					const data = JSON.parse(res.data)
					if(data.code == 200){
						resolve(data)
					}else{
						reject(data)
					}
				},
				fail: (err) => {
					// alert(JSON.stringify(err))
					console.log(err),
					reject(err)
				}
			})
		})
	return promise;
}


const api = {
	get:(url, data, header)=> requestApi(url, data, header,"GET"),
	post:(url, data, header)=> requestApi(url, data,header, "POST"),
	upfile:(url,data)=>upFileApi(url,data)
}

export default api


 