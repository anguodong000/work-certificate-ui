import api from '../request.js'

// 登录
export function login(data){
	return  api.post('/login',data)
}

// 登出
export function loginOut(data){
	return api.post('/logout', data)
}

// 注册
export function registerUser(data){
	return api.post('/register', data)
}

// 获取用户信息
export function getLoginUserInfo(data){
	return api.post('/user/getLoginUserInfo', data)
}

// 查询用户身份（用户/代理）
export function getUserRoleInfo(data){
	return api.post('/user/getUserRole', data)
}

// 发送短信验证码
export function sendPhoneCode(data){
	return api.post('/sendPhoneCode', data)
}
