import api from '../request.js'

// 保存考试报名的基本信息
export function saveWorkInfo(data){
	return api.post('/workCertificateInfo/save',data)
}

// 上传图片
export function uploadPicture(data){
	// const formData = new FormData();
	// formData.append('file', data)
	// console.log('formData', formData)
	return api.upfile('/uploadPicture',data)
}

// 分页查询作业证信息
export function getWorkInfoPage(data){
	return api.post('/workCertificateInfo/page',data)
}

// 单个查询作业信息
export function getWorkInfoId(id){
	return api.get(`/workCertificateInfo/getById?id=${id}`)
}
 

// 支付宝支付
export function payAlipay(data){
	return api.post('/alipay/trade/pay', data)
}
// 支付宝支付状态
export function getPayStatus(data){
	return api.post('/alipay/trade/getOrderState', data)
}
// 查询支付价格
export function getPriceById(data){
	return api.post('/alipay/trade/getPrice', data)
}
// 查询支付价格列表
export function getReceiveList(data){
	return api.post('/alipay/trade/getReceiveList', data)
}
// 用户绑定支付宝账号
export function bindAliAccount(data){
	return api.post('/user/bindAliAccount', data)
}
// 代理设置价格
export function setPrice(data){
	return api.post('/alipay/trade/setPrice', data)
}
// 查询报考记录
export function getPageByUserId(data){
	return api.post('/workCertificateInfo/pageByUserId', data)
}

// 收入统计
export function getReceiveSum(data){
	return api.post('/alipay/trade/getReceiveSum', data)
}