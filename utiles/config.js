export const educationsList = [
					{
						text: '初中',
						value: '初中',
					},{
						text: '高中',
						value: '高中',
					},{
						text: '职高',
						value: '职高',
					},{
						text: '专科',
						value: '专科',
					},{
						text: '本科',
						value: '本科',
					},{
						text: '研究生',
						value: '研究生',
					},{
						text: '博士',
						value: '博士',
					},
		]
		
export const sexsList = [{
					text: '男',
					value: 1
				}, {
					text: '女',
					value: 0
				}]
export const sexsObj ={
	0: '女',
	1: '男'
}
export const pointList = [{
					text: '石家庄市工业和信息化局职工培训中心',
					//value: '石家庄工信局培训中心考点'
					value:'石家庄市工业和信息化局职工培训中心'
				}]
				
export const subjectObj ={
	'LOW_PRESSURE_LICENCE_GENERAL':'低压电工作业-普班',
	'LOW_PRESSURE_LICENCE_VIP':'低压电工作业-VIP班',
	'HIGH_PRESSURE_LICENCE_GENERAL':'高压电工作业-普班',
	'HIGH_PRESSURE_LICENCE_VIP':'高压电工作业-VIP班',
	'WELD_THERMAL_CUTTING_LICENCE_GENERAL':'焊接与热切割作业-普班',
	'WELD_THERMAL_CUTTING_LICENCE_VIP':'焊接与热切割作业-VIP班',
	'HIGH_RISK_LICENCE_GENERAL':'高处作业-普班',
	'HIGH_RISK_LICENCE_VIP':'高处作业-VIP班',
	'REFRIGERATION_AIR_CONDITIONER_LICENCE_GENERAL':'制冷与空调作业-普班',
	'REFRIGERATION_AIR_CONDITIONER_LICENCE_VIP':'制冷与空调作业-VIP班',
	'SPECIAL_WORK_RE_EXAMINE':'特种作业操作证复审',
}
export const subjectObjById ={
	1:{
		key:'LOW_PRESSURE_LICENCE_GENERAL',
		name:'低压电工作业-普班',
	},
	2:{
		key:'LOW_PRESSURE_LICENCE_VIP',
		name:'低压电工作业-VIP班',
	},
	3:{
		key:'HIGH_PRESSURE_LICENCE_GENERAL',
		name:'高压电工作业-普班',
	},
	4:{
		key:'HIGH_PRESSURE_LICENCE_VIP',
		name:'高压电工作业-VIP班',
	},
	5:{
		key:'WELD_THERMAL_CUTTING_LICENCE_GENERAL',
		name:'焊接与热切割作业-普班',
	},
	6:{
		key:'WELD_THERMAL_CUTTING_LICENCE_VIP',
		name:'焊接与热切割作业-VIP班',
	},
	7:{
		key:'HIGH_RISK_LICENCE_GENERAL',
		name:'高处作业-普班',
	},
	8:{
		key:'HIGH_RISK_LICENCE_VIP',
		name:'高处作业-VIP班',
	},
	9:{
		key:'REFRIGERATION_AIR_CONDITIONER_LICENCE_GENERAL',
		name:'制冷与空调作业-普班',
	},
	10:{
		key:'REFRIGERATION_AIR_CONDITIONER_LICENCE_VIP',
		name:'制冷与空调作业-VIP班',
	},
	11:{
		key:'SPECIAL_WORK_RE_EXAMINE',
		name:'特种作业操作证复审',
	},
}
export const subjectList = [
	{
		id:1,
		key:'LOW_PRESSURE_LICENCE_GENERAL',
		name:'低压电工作业-普班',
	},
	{
		id:2,
		key:'LOW_PRESSURE_LICENCE_VIP',
		name:'低压电工作业-VIP班',
	},
	{
		id:3,
		key:'HIGH_PRESSURE_LICENCE_GENERAL',
		name:'高压电工作业-普班',
	},
	{
		id:4,
		key:'HIGH_PRESSURE_LICENCE_VIP',
		name:'高压电工作业-VIP班',
	},
	{
		id:5,
		key:'WELD_THERMAL_CUTTING_LICENCE_GENERAL',
		name:'焊接与热切割作业-普班',
	},
	{
		id:6,
		key:'WELD_THERMAL_CUTTING_LICENCE_VIP',
		name:'焊接与热切割作业-VIP班',
	},
	{
		id:7,
		key:'HIGH_RISK_LICENCE_GENERAL',
		name:'高处作业-普班',
	},
	{
		id:8,
		key:'HIGH_RISK_LICENCE_VIP',
		name:'高处作业-VIP班',
	},
	{
		id:9,
		key:'REFRIGERATION_AIR_CONDITIONER_LICENCE_GENERAL',
		name:'制冷与空调作业-普班',
	},
	{
		id:10,
		key:'REFRIGERATION_AIR_CONDITIONER_LICENCE_VIP',
		name:'制冷与空调作业-VIP班',
	},
	{
		id:11,
		key:'SPECIAL_WORK_RE_EXAMINE',
		name:'特种作业操作证复审',
	},
]

export const statusObj={
	'ARRANGED':'已排考试',
	'UNPAID':'未付款',
	'PAID': '已付款'
}